/*
Copyright 2021 FXcoder

This file is part of RHA.

RHA is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RHA is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with RHA. If not, see
http://www.gnu.org/licenses/.
*/

// Класс буфера для вычислений (DRAW_NONE, INDICATOR_CALCULATIONS). © FXcoder

/*
Число буферов:
	- MT5: 3
	- MT4, 6 (2C+2)
*/

#property strict

#include "../bsl.mqh"
#include "colorhistogram2.mqh"


class CBicolorHistogram2: public CColorHistogram2
{
protected:

	color bull_color_;
	color bear_color_;
	color back_color_;


public:

	void CBicolorHistogram2(int plot_index_first, int buffer_index_first):
		CColorHistogram2(plot_index_first, buffer_index_first, 2),
		bull_color_(clrNONE),
		bear_color_(clrNONE),
		back_color_(clrNONE)
	{
	}

	void CBicolorHistogram2(const CPlot &prev):
		CColorHistogram2(prev, 2),
		bull_color_(clrNONE),
		bear_color_(clrNONE),
		back_color_(clrNONE)
	{
	}


	virtual CBicolorHistogram2 *init(int plot_index_first, int buffer_index_first) override
	{
		CColorHistogram2::init(plot_index_first, buffer_index_first, 2);
		return(&this);
	}

	// true if changed
	bool set_colors(color bull_color, color bear_color)
	{
		validate_two_colors(bull_color, bear_color);

#ifdef __MQL4__

		color back_color = _chart.color_background();

		if ((bull_color == bull_color_) && (bear_color == bear_color_) && (back_color == back_color_))
			return(false);

		back_color_ = back_color;

#else

		if ((bull_color == bull_color_) && (bear_color == bear_color_))
			return(false);

#endif

		bull_color_ = bull_color;
		bear_color_ = bear_color;

		color colors[];
		_arr.add(colors, bull_color);
		_arr.add(colors, bear_color);
		CColorHistogram2::set_colors(colors);

#ifdef __MQL4__

		int mask_index = buffer_index_first_ + color_indexes_ * 2;// + 1;
		SetIndexStyle(mask_index, EMPTY, EMPTY, EMPTY, back_color);

#endif

		return(true);
	}
};
