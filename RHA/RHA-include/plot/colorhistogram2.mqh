/*
Copyright 2021 FXcoder

This file is part of RHA.

RHA is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RHA is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with RHA. If not, see
http://www.gnu.org/licenses/.
*/

// Класс цветной гистограммы на двух индикаторных буферах (DRAW_COLOR_HISTOGRAM2) с имитацией в 4. © FXcoder

/*
В 5 доступно из коробки как DRAW_COLOR_HISTOGRAM2 с двумя цветами. В 4 требуется имитация, причём методы
	разные для основного окна и дополнительного.

Имитация в основном окне в 4:

Необходимо по два буфера на каждый цвет. Пара буферов задаёт границы значений, порядок значения не имеет,
	(mki#54). Для N цветов - 2N буферов.

Имитация в дополнительном окне в 4:

Для имитации бара цвета нужны два буфера + 1 маска. Для N цветов - 2N буферов и 1 маска.
Два буфера нужны для случая, когда нижний край ниже 0, а верхний - выше (бар пересекает 0).

Желательно хранить цвет (как в 5 в отдельном буфере) в обоих случаях, это может упростить работу, кроме того
	в 5 эта возможность есть по умолчанию.

В 4 требуется сохранять чётность индекса первого буфера пары имитирующих гистограмм, поэтому общее число
	буферов лучше сделать чётным, даже если это добавит лишний пустой буфер.

Число буферов (C - число цветов):
	- MT5: 3
	- MT4, главное окно: 2C+1(цвет)+1(пустой)
	- MT4, доп. окно:    2C+1(маска)+1(цвет)
*/


#property strict

#include "plot.mqh"


class CColorHistogram2: public CPlot
{
protected:

#ifdef __MQL4__
	CBArrayDouble buffers1_[]; // size=colors_count
	CBArrayDouble buffers2_[]; // size=colors_count
	double mask_[];            // subwindow only
	double buffer_color_[];
	bool is_main_window_;
#else
	double buffer_high_[];
	double buffer_low_[];
	double buffer_color_[];
#endif


public:

	void CColorHistogram2(int plot_index_first, int buffer_index_first, int colors_count):
		CPlot()
	{
		init(plot_index_first, buffer_index_first, colors_count);
	}

	void CColorHistogram2(const CPlot &prev, int colors_count):
		CPlot()
	{
		init(prev.plot_index_next(), prev.buffer_index_next(), colors_count);
	}

	// по умолчанию 2 цвета
	virtual CColorHistogram2 *init(int plot_index_first, int buffer_index_first) override
	{
		init(plot_index_first, buffer_index_first, 2);
		return(&this);
	}

	// по умолчанию 2 цвета
	virtual CColorHistogram2 *init(const CPlot &prev) override
	{
		init(prev.plot_index_next(), prev.buffer_index_next(), 2);
		return(&this);
	}

	// Размер буфера (все одинаковой длины, любой подойдёт)
	virtual int size() const override
	{
		return(ArraySize(buffer_color_));
	}

	// bars - число последних баров для очистки, -1 = все
	virtual CColorHistogram2 *empty(int bars = -1) override
	{
		double empty_value = empty_value();

		if (bars < 0)
		{
#ifdef __MQL4__

			for (int i = 0; i < color_indexes_; i++)
			{
				buffers1_[i].fill(empty_value);
				buffers2_[i].fill(empty_value);
			}

			ArrayInitialize(buffer_color_, 0);
			ArrayInitialize(mask_, empty_value);
#else
			ArrayInitialize(buffer_high_,  empty_value);
			ArrayInitialize(buffer_low_,  empty_value);
			ArrayInitialize(buffer_color_, 0);
#endif
		}
		else
		{
			// empty может быть вызван до инициализации буферов и первого тика
			int size = size();

			if (bars > size)
				bars = size;

			int offset = size - bars;

#ifdef __MQL4__
			for (int i = 0; i < color_indexes_; i++)
			{
				buffers1_[i].fill(empty_value, offset, bars);
				buffers2_[i].fill(empty_value, offset, bars);
			}

			ArrayFill(buffer_color_, offset, bars, 0);
			ArrayFill(mask_, offset, bars, empty_value);
#else
			ArrayFill(buffer_high_,  offset, bars, empty_value);
			ArrayFill(buffer_low_,  offset, bars, empty_value);
			ArrayFill(buffer_color_, offset, bars, 0);
#endif
		}

		return(&this);
	}


	virtual CColorHistogram2 *empty_bar(int bar) override
	{
		double empty_value = empty_value();

#ifdef __MQL4__
		for (int i = 0; i < color_indexes_; i++)
		{
			buffers1_[i].data[bar] = empty_value;
			buffers2_[i].data[bar] = empty_value;
		}

		buffer_color_ [bar] = 0;
		mask_[bar] = empty_value;
#else
		buffer_high_       [bar] = empty_value;
		buffer_low_      [bar] = empty_value;
		buffer_color_ [bar] = 0;
#endif

		return(&this);
	}

	void set_colors(color &colors[])
	{
		CHECK_EXIT(ArraySize(colors) == color_indexes_);

		for (int i = 0; i < color_indexes_; i++)
		{
#ifdef __MQL4__
			line_color(2 * i + 0, colors[i]);
			line_color(2 * i + 1, colors[i]);
#else
			line_color(i, colors[i]);
#endif
		}
	}

	// return true if width changes
	bool update_auto_width()
	{
		static int width_prev = -1;

		int width = _chart.bar_body_line_width();
		if (width == width_prev)
			return(false);

#ifdef __MQL4__

		for (int i = 0; i < color_indexes_; i++)
		{
			SetIndexStyle(buffer_index_first_ + 2 * i, DRAW_HISTOGRAM, EMPTY, width);
			SetIndexStyle(buffer_index_first_ + 2 * i + 1, DRAW_HISTOGRAM, EMPTY, width);
		}

		if (!is_main_window_)
		{
			int mask_index = buffer_index_first_ + color_indexes_ * 2;// + 1;
			SetIndexStyle(mask_index, DRAW_HISTOGRAM, EMPTY, width);
		}

		width_prev = width;
#else
		PlotIndexSetInteger(plot_index_first_, PLOT_LINE_WIDTH, width);
#endif

		return(true);
	}

	void get_values(int bar, double &value1, double &value2)
	{
#ifdef __MQL4__
		int color_index = _math.round_to_int(buffer_color_[bar]);
		value1 = buffers1_[color_index].data[bar];
		value2 = buffers2_[color_index].data[bar];
#else
		value1 = buffer_high_[bar];
		value2 = buffer_low_[bar];
#endif
	}

	void set_values(int bar, double high, double low, int color_index)
	{
#ifdef __MQL4__

		// The order is important in subwindows.
		if (!is_main_window_ && (high < low))
			swap(high, low);

		int prev_color_index = (int)buffer_color_[bar];
		buffer_color_[bar] = color_index;

		int colors_count = color_indexes();

		for (int i = 0; i < colors_count; i++)
		{
			double empty_value = empty_value();
			buffers1_[i].data[bar] = empty_value;
			buffers1_[i].data[bar] = empty_value;
		}

		buffers1_[color_index].data[bar] = high;
		buffers2_[color_index].data[bar] = low;

		if (is_main_window_)
		{
			//mask_[bar] = empty_value();
		}
		else
		{
			if (low > 0)
			{
				mask_[bar] = low;
			}
			else if (high < 0)
			{
				mask_[bar] = high;
			}
			else
			{
				mask_[bar] = empty_value();
			}
		}

#else

		buffer_color_[bar] = color_index;
		buffer_high_[bar] = high;
		buffer_low_[bar] = low;

#endif

	}

#ifdef __MQL4__

	// Только для 4, в 5 встроенный метод работает нужным образом.
	virtual CColorHistogram2 *shift(int value) override
	{
		shift_ = value;

		for (int i = 0; i < buffer_index_count_; i++)
			SetIndexShift(buffer_index_first_ + i, value);

		return(&this);
	}

	virtual CColorHistogram2 *line_width(int value) override
	{
		line_width_ = value;

		for (int i = 0; i < buffer_index_count_; i++)
			SetIndexStyle(buffer_index_first_ + i, -1, -1, value);

		return(&this);
	}

#endif


protected:

	virtual CColorHistogram2 *init(int plot_index_first, int buffer_index_first, int colors_count)
	{
		// do not reinitialize

#ifdef __MQL4__

		/*
		В 4 необходима разная имитация для основного и дополнительного окон. В дополнительных нужна
			маска, в основном достаточно двух буферов.
		*/

		int subwindow = _indicator.window();
		CHECK(subwindow >= 0);
		is_main_window_ = subwindow == 0;

		plot_index_first_ = plot_index_first;
		plot_index_count_ = 2 * colors_count + 1 + 1;

		buffer_index_first_ = buffer_index_first;
		buffer_index_count_ = 2 * colors_count + 1 + 1;

		ArrayResize(buffers1_, colors_count);
		ArrayResize(buffers2_, colors_count);
		int width = _chart.bar_body_line_width();

		for (int i = 0; i < colors_count; i++)
		{
			int bi1 = buffer_index_first_ + 2 * i;
			int bi2 = buffer_index_first_ + 2 * i + 1;

			SetIndexBuffer(bi1, buffers1_[i].data);
			SetIndexBuffer(bi2, buffers2_[i].data);

			// mki#13
			ArraySetAsSeries(buffers1_[i].data, false);
			ArraySetAsSeries(buffers2_[i].data, false);

			SetIndexStyle(bi1, DRAW_HISTOGRAM, EMPTY, width);
			SetIndexStyle(bi2, DRAW_HISTOGRAM, EMPTY, width);

			SetIndexLabel(bi1, "value1[" + (string)i + "]");
			SetIndexLabel(bi2, "value2[" + (string)i + "]");
		}

		int next_index = buffer_index_first_ + colors_count * 2;

		// Этот буфер необязателен в главном окне, но лучше его создавать, чтобы была возможность
		//   использовать несколько таких гистограмм (число линий должно быть чётным).
		if (is_main_window_)
		{
			SetIndexBuffer(next_index, mask_);
			ArraySetAsSeries(mask_, false); // mki#13
			SetIndexStyle(next_index, DRAW_HISTOGRAM);
			SetIndexLabel(next_index, "");
			next_index++;
		}
		else
		{
			SetIndexBuffer(next_index, mask_);
			ArraySetAsSeries(mask_, false); // mki#13
			SetIndexStyle(next_index, DRAW_HISTOGRAM, EMPTY, EMPTY, _chart.color_background());
			SetIndexLabel(next_index, "mask");
			next_index++;
		}

		SetIndexBuffer(next_index, buffer_color_);
		ArraySetAsSeries(buffer_color_, false); // mki#13
		SetIndexStyle(next_index, DRAW_NONE);
		SetIndexLabel(next_index, "color");

#else

		plot_index_first_ = plot_index_first;
		plot_index_count_ = 1;

		buffer_index_first_ = buffer_index_first;
		buffer_index_count_ = 3;

		SetIndexBuffer(buffer_index_first_, buffer_high_, INDICATOR_DATA);
		SetIndexBuffer(buffer_index_first_ + 1, buffer_low_, INDICATOR_DATA);
		SetIndexBuffer(buffer_index_first_ + 2, buffer_color_, INDICATOR_COLOR_INDEX);

		draw_type(DRAW_COLOR_HISTOGRAM2); // после SetIndexBuffer

#endif

		color_indexes(colors_count);

		return(&this);
	}

};
