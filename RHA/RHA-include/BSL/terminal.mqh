/*
Copyright 2021 FXcoder

This file is part of RHA.

RHA is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RHA is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with RHA. If not, see
http://www.gnu.org/licenses/.
*/

// Terminal properties and functions. © FXcoder

#property strict

#include "type/single.mqh"
#include "type/uncopyable.mqh"

class CBTerminal: public CBUncopyable
{
public:

	// functions
	static bool close(int ret_code) { return(::TerminalClose(ret_code)); } // The function commands the terminal to complete operation

	// свойства int/bool
	static int  build                 () { return(        ::TerminalInfoInteger(TERMINAL_BUILD                 )); } // The client terminal build number
	static bool community_account     () { return( (bool) ::TerminalInfoInteger(TERMINAL_COMMUNITY_ACCOUNT     )); } // The flag indicates the presence of MQL5.community authorization data in the terminal
	static bool community_connection  () { return( (bool) ::TerminalInfoInteger(TERMINAL_COMMUNITY_CONNECTION  )); } // Connection to MQL5.community
	static int  codepage              () { return(        ::TerminalInfoInteger(TERMINAL_CODEPAGE              )); } // Number of the code page of the language installed in the client terminal
	static bool connected             () { return( (bool) ::TerminalInfoInteger(TERMINAL_CONNECTED             )); } // Connection to a trade server
	static int  cpu_cores             () { return(        ::TerminalInfoInteger(TERMINAL_CPU_CORES             )); } // The number of CPU cores in the system
	static int  disk_space            () { return(        ::TerminalInfoInteger(TERMINAL_DISK_SPACE            )); } // Free disk space for the MQL4\Files folder of the terminal, MB
	static bool dlls_allowed          () { return( (bool) ::TerminalInfoInteger(TERMINAL_DLLS_ALLOWED          )); } // Permission to use DLL
	static bool email_enabled         () { return( (bool) ::TerminalInfoInteger(TERMINAL_EMAIL_ENABLED         )); } // Permission to send e-mails using SMTP-server and login, specified in the terminal settings
	static bool ftp_enabled           () { return( (bool) ::TerminalInfoInteger(TERMINAL_FTP_ENABLED           )); } // Permission to send reports using FTP-server and login, specified in the terminal settings
	static int  max_bars              () { return(        ::TerminalInfoInteger(TERMINAL_MAXBARS               )); } // The maximal bars count on the chart
	static int  memory_available      () { return(        ::TerminalInfoInteger(TERMINAL_MEMORY_AVAILABLE      )); } // Free memory of the terminal process, MB
	static int  memory_physical       () { return(        ::TerminalInfoInteger(TERMINAL_MEMORY_PHYSICAL       )); } // Physical memory in the system, MB
	static int  memory_total          () { return(        ::TerminalInfoInteger(TERMINAL_MEMORY_TOTAL          )); } // Memory available to the process of the terminal, MB
	static int  memory_used           () { return(        ::TerminalInfoInteger(TERMINAL_MEMORY_USED           )); } // Memory used by the terminal, MB
	static bool mqid                  () { return( (bool) ::TerminalInfoInteger(TERMINAL_MQID                  )); } // The flag indicates the presence of MetaQuotes ID data to send Push notifications
	static bool notifications_enabled () { return( (bool) ::TerminalInfoInteger(TERMINAL_NOTIFICATIONS_ENABLED )); } // Permission to send notifications to smartphone
#ifdef __MQL4__
	static int  onencl_support        () { return(0); } // OpenCL is not supported in 4
#else
	static int  onencl_support        () { return(        ::TerminalInfoInteger(TERMINAL_OPENCL_SUPPORT        )); } // The version of the supported OpenCL in the format of 0x00010002 = 1.2.  "0" means that OpenCL is not supported
#endif
	static int  ping_last             () { return(        ::TerminalInfoInteger(TERMINAL_PING_LAST             )); } // The last known value of a ping to a trade server in microseconds
	static int  screen_dpi            () { return(        ::TerminalInfoInteger(TERMINAL_SCREEN_DPI            )); } // The resolution of information display (DPI)
	static bool trade_allowed         () { return( (bool) ::TerminalInfoInteger(TERMINAL_TRADE_ALLOWED         )); } // Permission to trade
#ifdef __MQL4__
	static bool x64                   () { return(false); } // 4 is always 32-bit
#else
	static bool x64                   () { return( (bool) ::TerminalInfoInteger(TERMINAL_X64                   )); } // Indication of the "64-bit terminal"
#endif

	// keys
	static int key_state_left       () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_LEFT     )); }
	static int key_state_up         () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_UP       )); }
	static int key_state_right      () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_RIGHT    )); }
	static int key_state_down       () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_DOWN     )); }
	static int key_state_shift      () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_SHIFT    )); }
	static int key_state_control    () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_CONTROL  )); }
	static int key_state_menu       () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_MENU     )); }
	static int key_state_capslock   () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_CAPSLOCK )); }
	static int key_state_numlock    () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_NUMLOCK  )); }
	static int key_state_scrlock    () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_SCRLOCK  )); }
	static int key_state_enter      () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_ENTER    )); }
	static int key_state_insert     () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_INSERT   )); }
	static int key_state_delete     () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_DELETE   )); }
	static int key_state_home       () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_HOME     )); }
	static int key_state_end        () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_END      )); }
	static int key_state_tab        () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_TAB      )); }
	static int key_state_pageup     () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_PAGEUP   )); }
	static int key_state_pagedown   () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_PAGEDOWN )); }
	static int key_state_escape     () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_ESCAPE   )); }

	// double properties
	static double community_balance () { return(::TerminalInfoDouble(TERMINAL_COMMUNITY_BALANCE)); } // Balance in MQL5.community

	// string properties
	static string language          () { return(::TerminalInfoString(TERMINAL_LANGUAGE        )); } // Language of the terminal
	static string company           () { return(::TerminalInfoString(TERMINAL_COMPANY         )); } // Company name
	static string name              () { return(::TerminalInfoString(TERMINAL_NAME            )); } // Terminal name
	static string path              () { return(::TerminalInfoString(TERMINAL_PATH            )); } // Folder from which the terminal is started
	static string data_path         () { return(::TerminalInfoString(TERMINAL_DATA_PATH       )); } // Folder in which terminal data are stored
	static string common_data_path  () { return(::TerminalInfoString(TERMINAL_COMMONDATA_PATH )); } // Common path for all of the terminals installed on a computer

	// additional functions and properties

	// keys
	static bool is_left_key_pressed       () { return(is_key_pressed(TERMINAL_KEYSTATE_LEFT     )); }
	static bool is_up_key_pressed         () { return(is_key_pressed(TERMINAL_KEYSTATE_UP       )); }
	static bool is_right_key_pressed      () { return(is_key_pressed(TERMINAL_KEYSTATE_RIGHT    )); }
	static bool is_down_key_pressed       () { return(is_key_pressed(TERMINAL_KEYSTATE_DOWN     )); }
	static bool is_shift_key_pressed      () { return(is_key_pressed(TERMINAL_KEYSTATE_SHIFT    )); }
	static bool is_control_key_pressed    () { return(is_key_pressed(TERMINAL_KEYSTATE_CONTROL  )); }
	static bool is_menu_key_pressed       () { return(is_key_pressed(TERMINAL_KEYSTATE_MENU     )); }
	static bool is_capslock_key_pressed   () { return(is_key_pressed(TERMINAL_KEYSTATE_CAPSLOCK )); }
	static bool is_numlock_key_pressed    () { return(is_key_pressed(TERMINAL_KEYSTATE_NUMLOCK  )); }
	static bool is_scrlock_key_pressed    () { return(is_key_pressed(TERMINAL_KEYSTATE_SCRLOCK  )); }
	static bool is_enter_key_pressed      () { return(is_key_pressed(TERMINAL_KEYSTATE_ENTER    )); }
	static bool is_insert_key_pressed     () { return(is_key_pressed(TERMINAL_KEYSTATE_INSERT   )); }
	static bool is_delete_key_pressed     () { return(is_key_pressed(TERMINAL_KEYSTATE_DELETE   )); }
	static bool is_home_key_pressed       () { return(is_key_pressed(TERMINAL_KEYSTATE_HOME     )); }
	static bool is_end_key_pressed        () { return(is_key_pressed(TERMINAL_KEYSTATE_END      )); }
	static bool is_tab_key_pressed        () { return(is_key_pressed(TERMINAL_KEYSTATE_TAB      )); }
	static bool is_pageup_key_pressed     () { return(is_key_pressed(TERMINAL_KEYSTATE_PAGEUP   )); }
	static bool is_pagedown_key_pressed   () { return(is_key_pressed(TERMINAL_KEYSTATE_PAGEDOWN )); }
	static bool is_escape_key_pressed     () { return(is_key_pressed(TERMINAL_KEYSTATE_ESCAPE   )); }


#ifndef __MQL4__

	// Properties

	// 5.1730
	static double retransmission  () { return(::TerminalInfoDouble(TERMINAL_RETRANSMISSION)); } // Percentage of resent network packets in the TCP/IP protocol for all running applications and services on the given computer

	// 5.1930
	static int screen_left   () { return(::TerminalInfoInteger(TERMINAL_SCREEN_LEFT  )); } // The left coordinate of the virtual screen
	static int screen_top    () { return(::TerminalInfoInteger(TERMINAL_SCREEN_TOP   )); } // The top coordinate of the virtual screen
	static int screen_width  () { return(::TerminalInfoInteger(TERMINAL_SCREEN_WIDTH )); } // Terminal width
	static int screen_height () { return(::TerminalInfoInteger(TERMINAL_SCREEN_HEIGHT)); } // Terminal height

	static int left   () { return(::TerminalInfoInteger(TERMINAL_LEFT  )); } // The left coordinate of the terminal relative to the virtual screen
	static int top    () { return(::TerminalInfoInteger(TERMINAL_TOP   )); } // The top coordinate of the terminal relative to the virtual screen
	static int right  () { return(::TerminalInfoInteger(TERMINAL_RIGHT )); } // The right coordinate of the terminal relative to the virtual screen
	static int bottom () { return(::TerminalInfoInteger(TERMINAL_BOTTOM)); } // The bottom coordinate of the terminal relative to the virtual screen

	// 5.2007
	static bool vps   () { return((bool)::TerminalInfoInteger(TERMINAL_VPS)); } // Indication that the terminal is launched on the MetaTrader MetaTrader VPS

#endif

	SINGLE_INSTANCE(CBTerminal)


protected:

	// Check if the key is pressed (not held down like *Lock keys, they sould be checked on the low bit)
	static bool is_key_pressed(ENUM_TERMINAL_INFO_INTEGER key) { return((::TerminalInfoInteger(key) & 0x80) != 0); }

	SINGLE_CONSTRUCTOR(CBTerminal)

};

SINGLE_GET(CBTerminal, _terminal)
