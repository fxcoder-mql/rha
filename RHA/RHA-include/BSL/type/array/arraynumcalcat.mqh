/*
Copyright 2021 FXcoder

This file is part of RHA.

RHA is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RHA is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with RHA. If not, see
http://www.gnu.org/licenses/.
*/

// Класс массива для числовых типов, абстрактная заготовка. © FXcoder

/*
Некоторые методы могли бы не быть абстрактными, но из-за бага в редакторе нет удобной
подсказки при использовании методов шаблонного класса, поэтому приходится их объявлять
явно в типизированных наследниках.
*/

#property strict

#include "arraynumat.mqh"


// Массив с функциями вычислений (вектор?)
// abstract
template <typename T>
class CBArrayNumCalcAT: public CBArrayNumAT<T>
{
public:

	void CBArrayNumCalcAT(): CBArrayNumAT() { }

	// abstract
	virtual void plus (T value)   = NULL;
	virtual void plus (T value, int first, int count) = NULL;
	virtual void mult (T value)   = NULL;
	virtual void mult (T value, int first, int count) = NULL;

};

