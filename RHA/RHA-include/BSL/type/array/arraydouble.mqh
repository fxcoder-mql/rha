/*
Copyright 2021 FXcoder

This file is part of RHA.

RHA is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RHA is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with RHA. If not, see
http://www.gnu.org/licenses/.
*/

// Класс массива double. © FXcoder

#property strict

#include "../../util/arr.mqh"
#include "../../util/math.mqh"
#include "arraynumcalcat.mqh"


class CBArrayDouble: public CBArrayNumCalcAT<double>
{
public:

	double data[];


public:

	void CBArrayDouble():                      CBArrayNumCalcAT() { }
	void CBArrayDouble(int size):              CBArrayNumCalcAT() { resize(size); }
	void CBArrayDouble(int size, int reserve): CBArrayNumCalcAT() { resize(size, reserve); }

	/* Implementation */

	virtual int size()                        override const { return(::ArraySize(data)); }
	virtual int resize(int size)              override       { return(::ArrayResize(data, size, reserve_)); };
	virtual int resize(int size, int reserve) override       { reserve_ = reserve; return(resize(size)); }

	virtual double operator[](int i) const override
	{
		if (i >= size())
			Print(VAR(i), VAR(size()));

		return(data[i]);
	}

	virtual int index_of(double value, int starting_from = 0) const override { return(_arr.index_of(data, value, starting_from)); }

	virtual void fill(double value) override { _arr.fill(data, value); }
	virtual void fill(double value, int first, int count) override { _arr.fill(data, value, first, count); }
	virtual int  add (double value) override { return(_arr.add(data, value, reserve_)); }

	virtual void clone(const double &src[]) override { _arr.clone(data, src); }
	virtual int  copy (const double &src[]) override { return(::ArrayCopy(data, src)); }
	virtual int  copy (const double &src[], int dst_start, int src_start, int count) override { return(::ArrayCopy(data, src, dst_start, src_start, count)); }

	virtual int    max_index(int first, int count) const override { return(_arr.max_index(data, first, count)); }
	virtual int    min_index(int first, int count) const override { return(_arr.min_index(data, first, count)); }
	virtual double max(int first, int count) const override { return(_math.max(data, first, count)); }
	virtual double min(int first, int count) const override { return(_math.min(data, first, count)); }

	virtual int    max_index() const override { return(_arr.max_index(data)); }
	virtual int    min_index() const override { return(_arr.min_index(data)); }
	virtual double max() const override { return(_math.max(data)); }
	virtual double min() const override { return(_math.min(data)); }

	virtual void plus(double value, int first, int count) override { _math.plus(data, value, first, count); }
	virtual void mult(double value, int first, int count) override { _math.mult(data, value, first, count); }

	virtual void plus(double value) override { _math.plus(data, value); }
	virtual void mult(double value) override { _math.mult(data, value); }

	virtual string to_string(string separator) const override { return(_arr.to_string(data, separator)); }

	/* Array and type-dependent functions */

	void operator=(const CBArrayDouble &rhs) { clone(rhs.data); }

	string to_string(string separator, int digits) const { return(_arr.to_string(data, separator, digits)); }

};
