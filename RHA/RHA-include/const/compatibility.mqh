/*
Copyright 2021 FXcoder

This file is part of RHA.

RHA is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RHA is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with RHA. If not, see
http://www.gnu.org/licenses/.
*/

// Константы для совместимости MQL4 и MQL5. © FXcoder

#property strict

#ifdef __MQL4__

#define SYMBOL_CALC_MODE_FOREX 0  // 0 взят из возвращаемых параметров MarketInfo (0 - Forex; 1 - CFD; 2 - Futures)
#define ENUM_DRAW_TYPE int

#else

#define EMPTY -1

#endif


