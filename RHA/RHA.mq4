/*
Copyright 2021 FXcoder

This file is part of RHA.

RHA is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RHA is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with RHA. If not, see
http://www.gnu.org/licenses/.
*/

#property copyright "RHA v4.0.1. © FXcoder"
#property link      "https://fxcoder.blogspot.com"
#property strict

#property description "Recursive Heiken Ashi."
#property indicator_chart_window

#ifdef __MQL4__
#property indicator_buffers 12+4
#else
#property indicator_buffers 6+4
#property indicator_plots 2+4
#endif

//#define DEBUG

#include "RHA-include/bsl.mqh"
#include "RHA-include/util/indicator.mqh"
#include "RHA-include/plot/bicolorhistogram2.mqh"
#include "RHA-include/plot/calcplot.mqh"


enum ENUM_HA_BAR_SYLE
{
	HA_BAR_SYLE_CANDLE             = 0x100,  // Candle
	HA_BAR_SYLE_CANDLE_SHADED      = 0x200,  // Candle with shaded tail

	HA_BAR_SYLE_TAIL_BAR           = 0x300,  // Narrow bar (High/Low)
	HA_BAR_SYLE_TAIL_BAR_SHADED    = 0x400,  // Narrow bar with shaded tail

	HA_BAR_SYLE_BODY_BAR           = 0x500,  // Wide bar (High/Low)
	HA_BAR_SYLE_BODY_BAR_SHADED    = 0x600,  // Wide bar with shaded tail

	HA_BAR_SYLE_NARROW_BODY        = 0x700,  // Narrow body (Open/Close)
	HA_BAR_SYLE_WIDE_BODY          = 0x800,  // Wide body (Open/Close)
};

bool EnumHABarStyleIsShaded(const ENUM_HA_BAR_SYLE chart_type)
{
	return(
		(chart_type == HA_BAR_SYLE_CANDLE_SHADED) ||
		(chart_type == HA_BAR_SYLE_TAIL_BAR_SHADED) ||
		(chart_type == HA_BAR_SYLE_BODY_BAR_SHADED)
		);
}

enum ENUM_HA_DEPTH
{
	HA_DEPTH_0        = 0,  // Original Price
	HA_DEPTH_1        = 1,  // Classic HA (x1)
	HA_DEPTH_2        = 2,  // HA(HA()) (x2)
	HA_DEPTH_3        = 3,  // HA(HA(...)) (x3)
	HA_DEPTH_5        = 5,  // HA(HA(...)) (x5)
	HA_DEPTH_8        = 8,  // HA(HA(...)) (x8)
	HA_DEPTH_13       = 13, // HA(HA(...)) (x13)
};


input ENUM_HA_DEPTH     Depth            = HA_DEPTH_13;                // Depth
input color             BullColor        = C'0,90,180';                // Bull Color (None = auto)
input color             BearColor        = C'180,0,0';                 // Bear Color (None = auto)
input ENUM_HA_BAR_SYLE  BarStyle         = HA_BAR_SYLE_CANDLE_SHADED;  // Bar Style
input double            ShadingPercent   = 50;                         // Shading Percent (0..100)
input bool              ColorOnly        = false;                      // Color Only


CCalcPlot plot_ha_open_(0, 0);
CCalcPlot plot_ha_high_(plot_ha_open_);
CCalcPlot plot_ha_low_(plot_ha_high_);
CCalcPlot plot_ha_close_(plot_ha_low_);

CBicolorHistogram2 plot_tail_(plot_ha_close_);
CBicolorHistogram2 plot_body_(plot_tail_);


int body_width_ = 0;
color color_bull_ = clrNONE;
color color_bear_ = clrNONE;
color color_shadow_bull_ = clrNONE;
color color_shadow_bear_ = clrNONE;
double shadow_mix_ = _math.limit(ShadingPercent / 100.0, 0.0, 1.0);
const bool is_tail_visible_ = (BarStyle != HA_BAR_SYLE_NARROW_BODY) && (BarStyle != HA_BAR_SYLE_WIDE_BODY);

CBState<color> bg_color_(clrNONE);


void OnInit()
{
	_indicator.short_name(_mql.program_name() + "(" + IntegerToString((int)Depth) + ")");
	update_view();
}

int OnCalculate(const int rates_total, const int prev_calculated, const datetime &time[], const double &open[], const double &high[], const double &low[], const double &close[], const long &tick_volume[], const long &volume[], const int &spread[])
{
	if (rates_total != plot_tail_.size()) // mki#50
		return(0);

#ifndef __MQL4__
	ArraySetAsSeries(open,  true);
	ArraySetAsSeries(high,  true);
	ArraySetAsSeries(low,   true);
	ArraySetAsSeries(close, true);
#endif

	int bars_to_calc = get_bars_to_calculate(rates_total, _Symbol, prev_calculated, 0, 1, 0);
	int first_bar = rates_total - 1;

	struct OHLC
	{
		double open, high, low, close;
	} bar, ha;

	for (int i = bars_to_calc - 1; i >= 0; --i)
 	{
 		const int pbar = first_bar - i;

		ha.open  = open[i];
		ha.high  = high[i];
		ha.low   = low[i];
		ha.close = close[i];

 		if (i != first_bar) // pbar > 0
 		{
			/*
			Formulas:

			ha.close = mean(open, close, high, low)
			ha.open  = mean(prev_open, prev_close)
			ha.high  = max(high, open, close)
			ha.low   = min(low, open, close)

			ha_open does not change.
			*/

 			if (Depth > 0)
 			{
				ha.open = (plot_ha_open_.buffer[pbar - 1] + plot_ha_close_.buffer[pbar - 1]) / 2.0;

				for (int d = 0; d < Depth; ++d)
				{
					const double new_ha_close = (ha.open + ha.high + ha.low + ha.close) / 4.0;
					ha.high  = _math.max(ha.high, ha.open, ha.close);
					ha.low   = _math.min(ha.low, ha.open, ha.close);
					ha.close = new_ha_close;
				}
			}
		}

		plot_ha_open_.buffer[pbar]  = ha.open;
		plot_ha_high_.buffer[pbar]  = ha.high;
		plot_ha_low_.buffer[pbar]   = ha.low;
		plot_ha_close_.buffer[pbar] = ha.close;

		bar.high  = is_tail_visible_ ? (ColorOnly ? high[i] : ha.high) : EMPTY_VALUE;
		bar.low   = is_tail_visible_ ? (ColorOnly ? low[i]  : ha.low)  : EMPTY_VALUE;
		bar.open  = ColorOnly ? open[i]  : ha.open;
		bar.close = ColorOnly ? close[i] : ha.close;

		const int color_index = ha.close < ha.open ? 1 : 0;

		plot_tail_.set_values(pbar, bar.low,  bar.high,  color_index);
		plot_body_.set_values(pbar, bar.open, bar.close, color_index);
 	}

	return(rates_total);
}

void OnChartEvent(const int id, const long& lparam, const double& dparam, const string& sparam)
{
	_chartevent.init(id, lparam, dparam, sparam);

	if (_chartevent.is_chart_change_event())
	{
		if (update_view())
			_chart.redraw();
	}
}

bool update_view()
{
	const bool is_body_width_updated = update_body_width();
	const bool is_colors_updated = update_colors();
	return(is_body_width_updated || is_colors_updated);
}

bool update_colors()
{
	bool is_updated = false;
	const bool update_tails = EnumHABarStyleIsShaded(BarStyle) && bg_color_.changed(_chart.color_background());

	// update bull color
	{
		const color new_bull_color = _color.validate(BullColor, _chart.color_chart_up());

		if ((new_bull_color != color_bull_) || update_tails)
		{
			color_bull_ = new_bull_color;
			color_shadow_bull_ = EnumHABarStyleIsShaded(BarStyle) ? _color.mix(_chart.color_background(), color_bull_, shadow_mix_) : color_bull_;
			is_updated = true;
		}
	}

	// update bear color
	{
		const color new_bear_color = _color.validate(BearColor, _chart.color_chart_down());

		if ((new_bear_color != color_bear_) || update_tails)
		{
			color_bear_ = new_bear_color;
			color_shadow_bear_ = EnumHABarStyleIsShaded(BarStyle) ? _color.mix(_chart.color_background(), color_bear_, shadow_mix_) : color_bear_;
			is_updated = true;
		}
	}

	if (is_updated)
	{
		plot_tail_.set_colors(color_shadow_bull_, color_shadow_bear_);
		plot_body_.set_colors(color_bull_, color_bear_);
	}

	return(is_updated);
}

bool update_body_width()
{
	const int new_body_width = _chart.bar_body_line_width();

	if (new_body_width == body_width_)
		return(false);

	body_width_ = new_body_width;

	if ((BarStyle == HA_BAR_SYLE_CANDLE) || (BarStyle == HA_BAR_SYLE_CANDLE_SHADED))
	{
		plot_body_.line_width(body_width_);
		plot_tail_.line_width(1);
	}
	else if ((BarStyle == HA_BAR_SYLE_TAIL_BAR) || (BarStyle == HA_BAR_SYLE_TAIL_BAR_SHADED) || (BarStyle == HA_BAR_SYLE_NARROW_BODY))
	{
		plot_body_.line_width(1);
		plot_tail_.line_width(1);
	}
	else if ((BarStyle == HA_BAR_SYLE_BODY_BAR) || (BarStyle == HA_BAR_SYLE_BODY_BAR_SHADED) || (BarStyle == HA_BAR_SYLE_WIDE_BODY))
	{
		plot_body_.line_width(body_width_);
		plot_tail_.line_width(body_width_);
	}

	return(true);
}

/*
Последние изменения:

4.0:
	* исправлено: При максимальном масштабе тело свечи меньше, чем тело оригинальных свеч (#4)
	* добавлен параметр Color Only, при включении будет меняться только цвет свечей, расположение как у оригинальных (#2)

3.2:
	* исправлено: стили Narrow Body и Wide Body работают неверно (#3)

3.1:
	* исправлено: ошибка в расчётах, по случайности не влияющая на результат

3.0:
	* первая публичная версия
*/
